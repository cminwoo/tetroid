# Tetroid
This program is a home-brew version of the classis Tetris game.

## Overview
This README will be organized as follows:

1. The features of the current build and how it works.
2. Additional features to be added in the future.
3. Concepts that were utilized in this project.
4. Credits and contact information.

## Features
The game runs just like any normal Tetris game. 

You are given a piece composed of four blocks organized in a set pattern, which is generated at the top of the window. You must guide the piece until it either touches the bottom or the top of another piece. You will then lose control of that piece and be given control of another.

If you fill an entire row with pieces, the line is removed and the score is incremented by 1 point. Every 10 points you accumulate, the level will increase by 1 and the speed at which the piece falls will increase (up until level 9).

If the pieces reach the top of the window, you lose, and the game resets.

A small picture illustrating this is given below:

![alt text](/resources/example.png "Tetris Example")

As can be seen in the image, we have the window containing the game on the left, with the name of the game, level, and score given to the right of it. Note that there are no grid lines. Consider this to be added to make it a little more difficult.

## Concepts
There are several concepts of C++ that are employed in this code. Note that many of them have been inserted artificially, and do little to optimize the code. Nonetheless, they help bring about a better understanding as to the mechanisms of each concept. To illustrate this, I will be giving the name of the concept and where it shows up in the code, with the actual code, if necessary.

### INHERITENCE
Qt is built on inheritence; objects in Qt are inherited from other classes.

### RAII
There are several objects in Qt that call **new** without a corresponding delete. What gives? This all falls under the fact that all the data called by **new** are encapsulated within a class; that is, all the data is dealt with within the destructor of that object, so no need to call **delete**. 

### CONTAINERS
In the program, I used the vector class. An example is given below:

``` c++
std::vector< std::vector<int> > current;
```

A vector is a container. Enough said.

### ITERATORS
Several vector iterators were employed in the code. An example is given below:

``` c++
bool tetroid::moves(const std::vector< std::vector<int> >& v, const int& x, const int& y)
{
    auto it = v.begin();
    for ( ; it != v.end(); ++it ) {
        if (board[it->at(1) / HEIGHT + y][it->at(0) / WIDTH + x] != 0) return true;
    }

    return false;
}
```
While iterators are helpful in producing generic code, since the iterator was used in a very specific, specialized code, it could also be done simply using integers, which can be considered "pseudo-iterators".

### FUNCTORS
Functors are very useful, as it allows us to pass a function in as a parameter. This is seen in the example below:

``` c++
class Row {
public:
    bool operator() (const int& val)
    {
        return (val != 0);
    }
} changeRow;
```

### LAMBDA FUNCTIONS
A lambda function in this program was used to produce a small function inline. This is useful since it won't bog up our namespace. It is as below:

``` c++
auto increment = [&](std::vector< std::vector<int> >& v, const int& sign) {
        for (size_t i = 0; i < 4; ++i) { v[i][0] = v[i][0] + sign * track_x; v[i][1] = v[i][1] + sign * track_y; }
    };
```

### GENERIC ALGORITHMS
A generic algorithm was used in this program. It was simply used in conjuction with the functor in order to count the number of elements in a vector that returned true when plugged into said functor. The code is given below:

```c++

size_t i = 0; i < 22; ++i) {
        int rows = std::count_if( board[i].begin() + 1, board[i].end() - 1, changeRow );
        if (rows == 10) rowDown(i);
    }
``` 

## Future Implementation
There are several features that I would like to implement in the future.

1. Sound effects. This includes both music and rotation sounds.
2. Grid lines for the board.
3. A way to "fast fall" the piece, so that it immediately goes to the bottom.
4. Some difficulty features (i.e. some randomizers like shifting lines to the right).
5. Generating an algorithm to detect which piece you need most and not give it to you.

None of these, unfortunately, will be implemented in time; however, I will return to the program and add these features soon.

## Credits
Inspiration was taking from the Qt Tetrix example.

Thank you also to Professor Salazar for his input regarding the code.

If there are any questions, please be sure to contact me. Thanks!

