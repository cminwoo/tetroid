#include "tetroid.h"
#include "ui_tetroid.h"

// Functor to check if cell contains something
class Row {
public:
    bool operator() (const int& val)
    {
        return (val != 0);
    }
} changeRow;

// Constructor
tetroid::tetroid(QWidget *parent) : QMainWindow(parent),
                                    ui(new Ui::tetroid)
{
    // Set up UI
    ui->setupUi(this);

    // Set up the board
    for (size_t i = 0; i < 23; ++i) {
        board[i][0]  = LEFT;
        board[i][11] = RIGHT;
    }
    for (size_t i = 0; i < 12; ++i) {
        board[22][i] = BOTTOM;
    }

    // Connect timeout to moveDown() function
    timer->start(speed);
    connect(timer, SIGNAL(timeout()), this, SLOT(moveDown()));

    // Set seed and initial shape
    qsrand(QTime::currentTime().msec());
    makeShape();
}

// Destructor
tetroid::~tetroid()
{
    delete ui;
}

// Set a random shape and assign to vectors
void tetroid::makeShape() // Inspired by Qt Tetrix
{
    shape = TetroidShape(qrand() % 7 + 1);

    // Have a lookup table for the various shapes
    static const int shapeCoord[8][4][2] =
    {
        { { 0, 0 },     { 0, 0 },   { 0, 0 },   { 0, 0 }    }, // NOSHAPE
        { { 0, 0 },     { 20, 0 },  { 0, 20 },  { 20, 20 }  }, // SQUARE
        { { -20, 0 },   { 0, 0 },   { 20, 0 },  { 0, 20 }   }, // T
        { { 20, -20 },  { 0, -20 }, { 0, 0 },   { 0, 20 }   }, // L_LEFT
        { { -20, -20 }, { 0, -20 }, { 0, 0 },   { 0, 20 }   }, // L_RIGHT
        { { 0, -20 },   { 0, 0 },   { 20, 0 },  { 20, 20 }  }, // SQUIGGLE_LEFT
        { { 0, -20 },   { 0, 0 },   { -20, 0 }, { -20, 20 } }, // SQUIGGLE_RIGHT
        { { 0, -20 },   { 0, 0 },   { 0, 20 },  { 0, 40 }   }  // LINE
    };

    // Create the block and increment to starting positions
    for (size_t i = 0; i < 4 ; i++) {
        current[i][0] = shapeCoord[shape][i][0] + track_x;
        current[i][1] = shapeCoord[shape][i][1] + track_y;
    }
}

// Draw each individual block in the piece
void tetroid::drawBlock(QPainter& painter, const int& x, const int& y) // Inspired by Qt Tetrix
{
    // Set the color options
    static const QRgb colorTable[8] =
    {
        0x000000, 0xCC6666, 0x66CC66, 0x6666CC,
        0xCCCC66, 0xCC66CC, 0x66CCCC, 0xDAAA00
    };

    // Create a single block
    QColor color = colorTable[int(shape)];
    painter.fillRect(x + 1, y + 1, WIDTH - 2, HEIGHT - 2, color);

    painter.setPen(color.light());
    painter.drawLine(x, y + HEIGHT - 1, x, y);
    painter.drawLine(x, y, x + WIDTH - 1, y);

    painter.setPen(color.dark());
    painter.drawLine(x + 1, y + HEIGHT - 1, x + WIDTH - 1, y + HEIGHT - 1);
    painter.drawLine(x + WIDTH - 1, y + HEIGHT - 1, x + HEIGHT - 1, y + 1);
}

void tetroid::paintEvent( QPaintEvent* event )
{
    QPainter painter(this);

    // Draw each piece in the shape
    for (size_t i = 0; i < 4; ++i)
        drawBlock(painter, current[i][0], current[i][1]);

    // Draw each block on the board
    for (size_t i = 0; i < 23; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (board[i][j] == PIECE)
                drawBlock(painter, 20 * j, 20 * i);
        }
    }
}

void tetroid::keyPressEvent( QKeyEvent* event )
{
    switch ( event->key() )
    {
        case Qt::Key_Left :
            moveLeft();
            break;
        case Qt::Key_Right :
            moveRight();
            break;
        case Qt::Key_Up :
            rotate();
            break;
        case Qt::Key_Down :
            moveDown();
            break;
        default :
            QWidget::keyPressEvent( event );
    }
}

void tetroid::moveLeft()
{
    if ( moves(current, (-1)) )
        return;

    for (size_t i = 0; i < 4; ++i)
        current[i][0] -= WIDTH;

    track_x -= WIDTH;

    update();
}

void tetroid::moveRight()
{
    if ( moves(current, 1) )
        return;

    for (size_t i = 0; i < 4; ++i)
        current[i][0] += WIDTH;
    track_x += WIDTH;

    update();
}

void tetroid::moveDown()
{
    if ( moves(current) )
        resetGame();

    else if ( moves(current, 0, 1) )
        resetPiece();

    else {
        for (size_t i = 0; i < 4; ++i)
            current[i][1] += HEIGHT;
        track_y += HEIGHT;
    }

    update();
}

void tetroid::rotate()
{
    auto increment = [&](std::vector< std::vector<int> >& v, const int& sign) {
        for (size_t i = 0; i < 4; ++i) { v[i][0] = v[i][0] + sign * track_x; v[i][1] = v[i][1] + sign * track_y; }
    };

    // If the shape is a square, do nothing
    if (shape == SQUARE)
        return;

    // Create a copy of the piece. This is to test the rotation
    for (size_t i = 0; i < 4; ++i) {
        next[i][0] = current[i][0];
        next[i][1] = current[i][1];
    }

    // Decrement to base array. This is so that we have an origin at (0, 0)
    increment( next, (-1) );

    // For each block, we want (x, y) -> (y, -x)
    // Do the rotation
    for (size_t i = 0; i < 4; ++i) {
        int block = next[i][0];
        next[i][0] = next[i][1];
        next[i][1] = -1 * block;
    }

    // Reincrement to previous position
    increment( next, 1 );

    // If out of bounds, try to move to the right
    if ( moves(next) ) {
        for (size_t i = 0; i < 4; ++i) next[i][0] += WIDTH;
        if ( moves(next) ) {
            for (size_t i = 0; i < 4; ++i) next[i][0] -= WIDTH;
        }
        else {
            for (size_t i = 0; i < 4; ++i) {
                current[i][0] = next[i][0];
                current[i][1] = next[i][1];
                }
        track_x += WIDTH;
        }
    }

    // If still out of bounds, try to move to the left
    if ( moves(next) ) {
        for (size_t i = 0; i < 4; ++i) next[i][0] -= WIDTH;
        if ( moves(next) ) {
            for (size_t i = 0; i < 4; ++i) next[i][0] += WIDTH;
        }
        else {
            for (size_t i = 0; i < 4; ++i) {
                current[i][0] = next[i][0];
                current[i][1] = next[i][1];
                }
        track_x -= WIDTH;
        }
    }

    // If not out of bounds, set blocks
    if ( !moves(next) ) {
        for (size_t i = 0; i < 4; ++i) {
            current[i][0] = next[i][0];
            current[i][1] = next[i][1];
        }
    }

    update();
}

// Check if any of the blocks are out of bounds
bool tetroid::moves(const std::vector< std::vector<int> >& v, const int& x, const int& y)
{
    auto it = v.begin();
    for ( ; it != v.end(); ++it ) {
        if (board[it->at(1) / HEIGHT + y][it->at(0) / WIDTH + x] != 0) return true;
    }

    return false;
}

// Adjusts speed
void tetroid::setSpeed()
{
    if (level > 9)
        return;

    speed -= 100;
    timer->start(speed);
}

// Moves rows down, increments score (and level)
void tetroid::rowDown(const int& row)
{
    for (size_t i = row; i > 0; --i) {
        for (int j = 1; j < 11; ++j) board[i][j] = board[i - 1][j];
    }

    ++score;
    ui->score->setText(QString::number(score));

    if ( score % 10 == 0 ) {
        ++level;
        ui->level->setText(QString::number(level));
        setSpeed();
    }
}

// Save where the piece was, then create a new piece at default location
void tetroid::resetPiece()
{
    for (size_t i = 0; i < 4; ++i)
        board[current[i][1] / 20][current[i][0] / 20] = PIECE;

    for (size_t i = 0; i < 22; ++i) {
        int rows = std::count_if( board[i].begin() + 1, board[i].end() - 1, changeRow );
        if (rows == 10) rowDown(i);
    }

    track_x = START_X;
    track_y = START_Y;

    makeShape();
    update();
}

// Resets everything
void tetroid::resetGame()
{
    score = 0;
    level = 0;
    speed = 1000;

    for (size_t i = 0; i < 22; ++i) {
        for (int j = 1; j < 11; ++j) board[i][j] = 0;
    }

    ui->score->setText(QString::number(score));
    ui->level->setText(QString::number(level));

    timer->start(speed);

    track_x = START_X;
    track_y = START_Y;

    makeShape();
    update();
}

void tetroid::update()
{
    QCoreApplication::processEvents();
    repaint();
}
