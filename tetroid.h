#ifndef TETROID_H
#define TETROID_H

#include <QMainWindow>
#include <QPainter>
#include <QKeyEvent>
#include <QTimer>
#include <QTime>

#include <vector>
#include <algorithm>

enum TetroidShape { NOSHAPE, SQUARE, T, L_LEFT, L_RIGHT, SQUIGGLE_LEFT, SQUIGGLE_RIGHT, LINE };
enum Borders      { LEFT = 1, RIGHT = 2, BOTTOM = 3, PIECE = 4 };

namespace Ui { class tetroid; }

class tetroid : public QMainWindow
{
    Q_OBJECT

public:
    explicit tetroid(QWidget *parent = 0);
    ~tetroid();
    void makeShape();

    void rotate();
    void moveLeft();
    void moveRight();

    bool moves(const std::vector< std::vector<int> >& v, const int& x = 0, const int& y = 0);

private slots:
    void moveDown();

private:
    void drawBlock(QPainter& painter, const int& x, const int& y);
    void paintEvent( QPaintEvent* event );
    void keyPressEvent( QKeyEvent* event );

    void setSpeed();
    void rowDown(const int& row);
    void resetPiece();
    void resetGame();

    void update();

    std::vector< std::vector<int> > current = std::vector< std::vector<int> >( 4, std::vector<int>(2) );
    std::vector< std::vector<int> > next    = std::vector< std::vector<int> >( 4, std::vector<int>(2) );
    std::vector< std::vector<int> > board   = std::vector< std::vector<int> >(23, std::vector<int>(12));

    const int WIDTH = 20;
    const int HEIGHT  = 20;
    const int START_X = 100;
    const int START_Y = 40;

    TetroidShape shape = NOSHAPE;
    int track_x = START_X;
    int track_y = START_Y;
    int score = 0;
    int level = 0;
    int speed = 1000;
    QTimer* timer = new QTimer(this);

    Ui::tetroid *ui;
};

#endif // TETROID_H
